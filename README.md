# baidu-trans-cli

> [Pypi: home page](https://pypi.org/project/baidu-trans-cli/)

## install 

1. pip install baidu-trans-cli

## get a key 

1. register a account on [this site](http://api.fanyi.baidu.com/api/trans/product/index) `2000,000 characters per month is free.`

1. get key and secretID, write into `~/.config/app-conf/baidu-trans/main.json`


## Use
> baidu-trans -h 


