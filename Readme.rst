Baidu Translation
=================

this is a python translation package based on baidu translation api.

**Installation**

.. code:: bash

    pip install baidu-trans-cli

1. register a account on http://api.fanyi.baidu.com/api/trans/product/index **2000,000 characters per month is free.**
2. get key and secretID, write into **~/.config/app-conf/baidu-trans/main.json**

**Usage**

.. code:: bash
    
    baidu-trans -h
